package fr.william.antennesmobile.activity

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import fr.william.antennesmobile.R
import fr.william.antennesmobile.fragment.MapFragment
import fr.william.antennesmobile.fragment.SettingsFragment
import fr.william.antennesmobile.fragment.SitesFragment
import fr.william.antennesmobile.utils.RefreshAsyncTask
import fr.william.antennesmobile.utils.Singleton.filterList
import fr.william.antennesmobile.utils.Singleton.filterMap
import fr.william.antennesmobile.utils.Singleton.mapList
import fr.william.antennesmobile.utils.Singleton.mapListFilter
import fr.william.antennesmobile.utils.Singleton.opIsVisibleList
import fr.william.antennesmobile.utils.Singleton.opIsVisibleMap
import fr.william.antennesmobile.utils.Singleton.operators
import fr.william.antennesmobile.utils.Singleton.rowSize
import fr.william.antennesmobile.utils.Singleton.sitesList
import fr.william.antennesmobile.utils.Singleton.sitesListFilter
import github.com.st235.lib_expandablebottombar.ExpandableBottomBar
import java.io.ObjectOutputStream

class MainActivity : AppCompatActivity(), RefreshAsyncTask, SearchView.OnQueryTextListener {

    private lateinit var currFragment: Fragment
    private lateinit var searchView: SearchView
    private lateinit var mapFragment: MapFragment
    private lateinit var sitesFragment: SitesFragment
    private lateinit var settingsFragment: SettingsFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        searchView = findViewById(R.id.searchView)
        searchView.setOnQueryTextListener(this)
        val searchIcon: ImageView = searchView.findViewById(androidx.appcompat.R.id.search_mag_icon)
        searchIcon.setColorFilter(Color.WHITE)
        val txtSearch = searchView.findViewById<View>(androidx.appcompat.R.id.search_src_text) as EditText
        txtSearch.setHintTextColor(Color.WHITE)
        txtSearch.setTextColor(Color.WHITE)

        sitesFragment = SitesFragment()
        mapFragment = MapFragment()
        settingsFragment = SettingsFragment()

        currFragment = sitesFragment
        supportFragmentManager.beginTransaction().add(R.id.flFragment, mapFragment, "2").hide(mapFragment).commit()
        supportFragmentManager.beginTransaction().add(R.id.flFragment, settingsFragment, "3").hide(settingsFragment).commit()
        supportFragmentManager.beginTransaction().add(R.id.flFragment, sitesFragment, "1").commit()

        val bottomNavigationView: ExpandableBottomBar = findViewById(R.id.bottomNavigationView)

        bottomNavigationView.onItemSelectedListener = { view, _, _ ->
            when (view.id) {
                R.id.sites -> {
                    shiftRecyclerView(0f)
                    searchView.visibility = View.GONE
                    supportFragmentManager.beginTransaction().hide(currFragment).show(sitesFragment).commit()
                    currFragment = sitesFragment
                    title = "Liste des sites"
                }
                R.id.map -> {
                    shiftRecyclerView(0f)
                    searchView.visibility = View.GONE
                    supportFragmentManager.beginTransaction().hide(currFragment).show(mapFragment).commit()
                    currFragment = mapFragment
                    title = "Map des sites"
                }
                R.id.settings -> {
                    shiftRecyclerView(0f)
                    searchView.visibility = View.GONE
                    supportFragmentManager.beginTransaction().hide(currFragment).show(settingsFragment).commit()
                    currFragment = settingsFragment
                    title = "Réglages"
                }
            }
        }
    }

    private fun customAlertDialogOperator() {
        val builder = AlertDialog.Builder(this, R.style.CustomAlertDialog)
        builder.setTitle("Opérateurs à afficher")

        if (mapFragment.isVisible) {
            builder.setMultiChoiceItems(operators, opIsVisibleMap) { _, pos, isChecked ->
                opIsVisibleMap[pos] = isChecked
            }
        } else {
            builder.setMultiChoiceItems(operators, opIsVisibleList) { _, pos, isChecked ->
                opIsVisibleList[pos] = isChecked
            }
        }

        builder.setPositiveButton("Valider") { dialog, _ ->
            if (mapFragment.isVisible) {
                mapListFilter.clear()
                mapListFilter = mapList.toMutableList()
                filterMap()
            } else {
                sitesListFilter.clear()
                sitesListFilter = sitesList.toMutableList()
                filterList()
            }
            dialog.dismiss()
            refreshFragment(false)
        }
        builder.setNegativeButton("Annuler", null)

        val dialog = builder.create()
        dialog.show()
    }

    private fun shiftRecyclerView(marginTop: Float) {
        val frameLayout = findViewById<FrameLayout>(R.id.flFragment)
        val param = frameLayout.layoutParams as ViewGroup.MarginLayoutParams
        param.setMargins(0, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, marginTop, resources.displayMetrics).toInt(), 0, 0)
        frameLayout.layoutParams = param
    }

    private fun refreshFragment(boolean: Boolean) {
        mapFragment.refresh(boolean)
        sitesFragment.refresh(boolean)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.top_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.location_menu -> {
                if (searchView.visibility == View.VISIBLE) {
                    if (sitesFragment.isVisible)
                        shiftRecyclerView(0f)
                    searchView.visibility = View.GONE
                } else {
                    if (sitesFragment.isVisible)
                        shiftRecyclerView(60f)
                    searchView.visibility = View.VISIBLE
                }
                true
            }
            R.id.filter_menu -> {
                customAlertDialogOperator()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun refresh(isChangingPosition: Boolean) {
        if (isChangingPosition) {
            sitesList = mapList.subList(0, rowSize)
        }
        sitesListFilter = sitesList.toMutableList()
        refreshFragment(isChangingPosition)
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        searchView.setQuery("", false)
        if (mapFragment.isVisible) {
            mapFragment.changeLocation(query)
        }
        if (sitesFragment.isVisible) {
            shiftRecyclerView(0f)
            sitesFragment.changeLocation(query)
        }
        searchView.visibility = View.GONE
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

    override fun onStop() {
        try {
            val fos = openFileOutput("savedFile", Context.MODE_PRIVATE)
            val out = ObjectOutputStream(fos)
            out.writeObject(mapList)
            out.close()
            fos.close()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        super.onStop()
    }
}