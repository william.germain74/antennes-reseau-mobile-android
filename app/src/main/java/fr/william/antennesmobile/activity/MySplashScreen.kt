package fr.william.antennesmobile.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import fr.william.antennesmobile.R
import fr.william.antennesmobile.utils.HttpConnectServerAsyncTask
import fr.william.antennesmobile.utils.RefreshAsyncTask
import fr.william.antennesmobile.utils.Singleton
import fr.william.antennesmobile.utils.Singleton.distance
import fr.william.antennesmobile.utils.Singleton.getUrl
import fr.william.antennesmobile.utils.Singleton.isConnectedToInternet
import fr.william.antennesmobile.utils.Singleton.mapList
import fr.william.antennesmobile.utils.Singleton.mapListFilter
import fr.william.antennesmobile.utils.Singleton.setCoordinates
import fr.william.antennesmobile.utils.Singleton.setCurrentCoordinates
import fr.william.antennesmobile.utils.Singleton.sitesList
import fr.william.antennesmobile.utils.Singleton.sitesListFilter
import fr.william.antennesmobile.utils.Singleton.wantToDisplay2G
import fr.william.antennesmobile.utils.Singleton.wantToDisplay3G
import fr.william.antennesmobile.utils.Singleton.wantToDisplay4G
import fr.william.antennesmobile.utils.Singleton.wantToDisplay5G
import fr.william.antennesmobile.utils.Sites
import java.io.File
import java.io.FileInputStream
import java.io.ObjectInputStream

@SuppressLint("CustomSplashScreen")
class MySplashScreen : AppCompatActivity(), RefreshAsyncTask {

    private var isDisplay = true
    private var fusedLocationClient: FusedLocationProviderClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        if (supportActionBar != null) supportActionBar?.hide()

        loadSharedPreferences()

        if (checkForInternet(this)) getLocation() else showAlertDialog()

        YoYo.with(Techniques.Tada).delay(500).duration(1000).playOn(findViewById(R.id.splashlogo))
    }

    private fun loadSharedPreferences() {
        val sharedPreferences = this.getSharedPreferences("MyPreferences", MODE_PRIVATE)
        distance = sharedPreferences.getInt("distance", 15000)
        wantToDisplay2G = sharedPreferences.getBoolean(R.id.switch_2g.toString(), true)
        wantToDisplay3G = sharedPreferences.getBoolean(R.id.switch_3g.toString(), true)
        wantToDisplay4G = sharedPreferences.getBoolean(R.id.switch_4g.toString(), true)
        wantToDisplay5G = sharedPreferences.getBoolean(R.id.switch_5g.toString(), true)
    }

    private fun checkForInternet(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false

            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            @Suppress("DEPRECATION") val networkInfo = connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

    private fun showAlertDialog() {
        isConnectedToInternet = false
        val builder = AlertDialog.Builder(this, R.style.CustomAlertDialog)
        builder.setTitle("Pas de connexion !")
        builder.setCancelable(false)
        builder.setMessage("Il semblerait que vous ne soyez pas connecter à internet, voulez-vous charger les derniers sites enregistrés ou quitter l'application ?")

        builder.setPositiveButton("Charger") { dialog, _ ->
            dialog.dismiss()
            loadLastSitesList()
        }

        builder.setNegativeButton("Quitter") { dialog, _ ->
            dialog.dismiss()
            finish()
        }

        builder.show()
    }

    private fun loadLastSitesList() {
        val sharedPreferences = this.getSharedPreferences("MyPreferences", MODE_PRIVATE)
        val lat = sharedPreferences.getString("latitude", "46.2051675")?.toDouble()
        val long = sharedPreferences.getString("longitude", "5.2255007")?.toDouble()
        if (lat != null && long != null) {
            setCoordinates(lat, long)
            setCurrentCoordinates(lat, long)
        }

        if (File(this.filesDir, "savedFile").exists()) {
            try {
                val fis: FileInputStream = openFileInput("savedFile")
                val open = ObjectInputStream(fis)
                mapList = open.readObject() as MutableList<Sites>
                open.close()
                fis.close()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        refresh(false)
    }

    private fun getLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 22)
        } else {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            if (fusedLocationClient !== null) {
                val test = fusedLocationClient?.lastLocation
                test?.addOnSuccessListener { location ->
                    if (location != null) {
                        val sharedPreferences = this.getSharedPreferences("MyPreferences", MODE_PRIVATE)
                        sharedPreferences.edit().putString("latitude", location.latitude.toString()).apply()
                        sharedPreferences.edit().putString("longitude", location.longitude.toString()).apply()
                        setCoordinates(location.latitude, location.longitude)
                        setCurrentCoordinates(location.latitude, location.longitude)
                        startAsyncTask()
                    }
                }

                test?.addOnFailureListener { e ->
                    Log.d("Willy", "Erreur localisation : $e | ${e.message}")
                    Toast.makeText(this, "Erreur : ${e.message}", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun startAsyncTask() {
        HttpConnectServerAsyncTask(this).execute(
            getUrl(true).plus("&geofilter.distance=${Singleton.lastLatitude}, ${Singleton.lastLongitude}, $distance"),
            true,
            false
        )
    }

    override fun refresh(isChangingPosition: Boolean) {
        sitesList = mapList.subList(0, Singleton.rowSize)
        isDisplay = false
        mapListFilter = mapList.toMutableList()
        sitesListFilter = sitesList.toMutableList()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 22) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) getLocation()
            else Toast.makeText(this, "Permission refusée !", Toast.LENGTH_SHORT).show()
        }
    }
}