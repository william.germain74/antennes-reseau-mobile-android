package fr.william.antennesmobile.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import fr.william.antennesmobile.R
import fr.william.antennesmobile.utils.Singleton.currentLatitude
import fr.william.antennesmobile.utils.Singleton.currentLongitude
import fr.william.antennesmobile.utils.Sites
import fr.william.antennesmobile.utils.Singleton.lastLatitude
import fr.william.antennesmobile.utils.Singleton.lastLongitude
import kotlin.math.ln


class SiteInformation : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var site: Sites
    private lateinit var tOperator: TextView
    private lateinit var tAddress1: TextView
    private lateinit var tAddress2: TextView
    private lateinit var tDistance: TextView
    private lateinit var tTechnologies: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_site_information)

        title = "Information sur le site"

        tOperator = findViewById(R.id.info_operator)
        tAddress1 = findViewById(R.id.info_address)
        tAddress2 = findViewById(R.id.info_address2)
        tDistance = findViewById(R.id.info_distance)
        tTechnologies = findViewById(R.id.info_technology)

        site = intent.extras?.getSerializable("site") as Sites

        tOperator.text = site.op_name.plus(" (${site.op_site_id})")
        tAddress1.text = site.com_code.plus(" ").plus(site.com_name)
        tAddress2.text = site.dep_code.plus(" ").plus(site.dep_name).plus(" ").plus(site.reg_name)
        tDistance.text = site.distance
        tTechnologies.text = site.technology.replace(",", " ")

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun distanceInMeters(dist: String): Double {
        var sDistance = dist
        return if (sDistance.contains("km")) {
            sDistance = sDistance.replace(" km", "").replace(",", ".")
            sDistance.toDouble() * 1000 + 50
        } else {
            sDistance = sDistance.replace(" m", "")
            sDistance.toDouble() + 50
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(gMap: GoogleMap) {
        var lattng = LatLng(lastLatitude, lastLongitude)
        if(currentLatitude != lastLatitude && currentLongitude != lastLongitude)
            gMap.addMarker(MarkerOptions().position(lattng).title("Vous etes ici !"))
        gMap.isMyLocationEnabled = true
        val zoomLevel = (ln(40000 / (distanceInMeters(site.distance) * 3.5 * 2 / 1000 / 2)) / ln(2.0)).toFloat()
        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lattng, zoomLevel))
        lattng = LatLng(site.location.latitude, site.location.longitude)
        gMap.addMarker(MarkerOptions().position(lattng).title(site.op_name))
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
}