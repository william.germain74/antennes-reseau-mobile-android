package fr.william.antennesmobile.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.william.antennesmobile.R
import fr.william.antennesmobile.utils.Singleton.sitesListFilter
import fr.william.antennesmobile.utils.Singleton.wantToDisplay2G
import fr.william.antennesmobile.utils.Singleton.wantToDisplay3G
import fr.william.antennesmobile.utils.Singleton.wantToDisplay4G
import fr.william.antennesmobile.utils.Singleton.wantToDisplay5G
import java.io.Serializable

class SitesAdapter(private val cellClickListener: CellClickListener) :
    RecyclerView.Adapter<SitesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val site = sitesListFilter[position]

        if (site.technology.contains("2G") && wantToDisplay2G) holder.img2g.visibility =
            View.VISIBLE else holder.img2g.visibility = View.GONE
        if (site.technology.contains("3G") && wantToDisplay3G) holder.img3g.visibility =
            View.VISIBLE else holder.img3g.visibility = View.GONE
        if (site.technology.contains("4G") && wantToDisplay4G) holder.img4g.visibility =
            View.VISIBLE else holder.img4g.visibility = View.GONE
        if (site.technology.contains("5G") && wantToDisplay5G) holder.img5g.visibility =
            View.VISIBLE else holder.img5g.visibility = View.GONE

        holder.operatorName.text = site.op_name
        holder.communeName.text = site.com_name
        holder.distance.text = site.distance

        holder.itemView.setOnClickListener {
            cellClickListener.onCellClickListener(position)
        }
    }

    override fun getItemCount(): Int {
        return sitesListFilter.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img2g: ImageView = view.findViewById(R.id.img_2g)
        val img3g: ImageView = view.findViewById(R.id.img_3g)
        val img4g: ImageView = view.findViewById(R.id.img_4g)
        val img5g: ImageView = view.findViewById(R.id.img_5g)
        val operatorName: TextView = view.findViewById(R.id.operator_name)
        val communeName: TextView = view.findViewById(R.id.commune_name)
        val distance: TextView = view.findViewById(R.id.distance)
    }
}

interface CellClickListener {
    fun onCellClickListener(position: Int)
}