package fr.william.antennesmobile.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterManager
import fr.william.antennesmobile.R
import fr.william.antennesmobile.activity.SiteInformation
import fr.william.antennesmobile.utils.CustomClusterRenderer
import fr.william.antennesmobile.utils.HttpConnectServerAsyncTask
import fr.william.antennesmobile.utils.RefreshAsyncTask
import fr.william.antennesmobile.utils.Singleton.distance
import fr.william.antennesmobile.utils.Singleton.filterMap
import fr.william.antennesmobile.utils.Singleton.getUrl
import fr.william.antennesmobile.utils.Singleton.lastLatitude
import fr.william.antennesmobile.utils.Singleton.lastLongitude
import fr.william.antennesmobile.utils.Singleton.mapList
import fr.william.antennesmobile.utils.Singleton.mapListFilter
import fr.william.antennesmobile.utils.Sites
import java.io.IOException
import kotlin.math.ln

class MapFragment : Fragment(), OnMapReadyCallback, RefreshAsyncTask {

    private var gMap: GoogleMap? = null
    private lateinit var fragView: View
    private var alertDialog: AlertDialog? = null
    private var btnLegendMenu: ImageView? = null
    private var menuLegendClose: ImageView? = null
    private var menuLegendOpen: ConstraintLayout? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragView = inflater.inflate(R.layout.fragment_map, container, false)

        btnLegendMenu = fragView.findViewById(R.id.button_menu_legend)
        menuLegendOpen = fragView.findViewById(R.id.menu_legend_open)
        menuLegendClose = fragView.findViewById(R.id.menu_legend_close)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        btnLegendMenu?.setOnClickListener { _ ->
            openAndCloseLegendMenu()
        }

        menuLegendClose?.setOnClickListener { _ ->
            if (menuLegendClose?.visibility == View.VISIBLE)
                openAndCloseLegendMenu()
        }

        return fragView
    }

    private fun openAndCloseLegendMenu() {
        if (menuLegendOpen?.visibility == View.VISIBLE) {
            btnLegendMenu?.setImageResource(R.drawable.ic_expand)
            menuLegendOpen?.visibility = View.INVISIBLE
            menuLegendClose?.visibility = View.VISIBLE
        } else {
            btnLegendMenu?.setImageResource(R.drawable.ic_reduce)
            menuLegendOpen?.visibility = View.VISIBLE
            menuLegendClose?.visibility = View.INVISIBLE
        }
    }

    fun changeLocation(query: String) {
        var addressList = listOf<Address>()

        if (query !== "") {
            val geocoder = Geocoder(activity)
            try {
                addressList = geocoder.getFromLocationName(query, 1)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            if (addressList.isNotEmpty()) {
                val address = addressList[0]
                val builder = AlertDialog.Builder(activity as Context)
                builder.setTitle("Changer de ville ?")
                builder.setCancelable(false)
                builder.setMessage("Choisir la ville ${address.getAddressLine(0)} ${address.adminArea} ?")
                builder.setPositiveButton("Oui") { dialog, _ ->
                    lastLatitude = address.latitude
                    lastLongitude = address.longitude
                    dialog.dismiss()
                    updateSites()
                    showAlertDialog()
                }
                builder.setNegativeButton("Non") { dialog, _ ->
                    dialog.cancel()
                }
                builder.show()
            }
        }
    }

    private fun showAlertDialog() {
        val loading = AlertDialog.Builder(activity as Context, R.style.CustomAlertDialog)
        loading.setCancelable(false)
        val view = LayoutInflater.from(activity as Context).inflate(R.layout.custom_progress_bar, null)
        loading.setView(view)
        alertDialog = loading.create()
        alertDialog?.show()
    }

    private fun updateSites() {
        HttpConnectServerAsyncTask(activity).execute(
            getUrl(true).plus("&geofilter.distance=${lastLatitude}, ${lastLongitude}, $distance"),
            false,
            false
        )
    }

    private fun displayClusters() {
        gMap?.clear()
        val clusterManager = ClusterManager<Sites>(activity, gMap)
        clusterManager.setAnimation(false)
        gMap?.setOnCameraIdleListener(clusterManager)
        clusterManager.addItems(mapListFilter)
        clusterManager.renderer = CustomClusterRenderer(activity as Context, gMap, clusterManager)
        clusterManager.cluster()
        clusterManager.setOnClusterItemInfoWindowClickListener { site ->
            val intent = Intent(activity as Context, SiteInformation::class.java)
            intent.putExtra("site", findSite(site.position.latitude, site.position.longitude))
            startActivity(intent)
            activity?.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        gMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lastLatitude, lastLongitude), (ln(20000000.0 / distance) / ln(2.0)).toFloat()))
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.isMyLocationEnabled = true
        gMap = googleMap
        displayClusters()
    }

    private fun findSite(latitude: Double, longitude: Double): Sites? {
        return mapListFilter.find { it.location.latitude == latitude && it.location.longitude == longitude }
    }

    override fun refresh(isChangingPosition: Boolean) {
        if(alertDialog?.isShowing == true)
            alertDialog?.dismiss()
        mapListFilter = mapList.toMutableList()
        filterMap()
        displayClusters()
    }
}