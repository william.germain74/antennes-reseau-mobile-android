package fr.william.antennesmobile.fragment

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.text.InputType
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import fr.william.antennesmobile.R
import fr.william.antennesmobile.utils.Singleton.distance
import fr.william.antennesmobile.utils.Singleton.wantToDisplay2G
import fr.william.antennesmobile.utils.Singleton.wantToDisplay3G
import fr.william.antennesmobile.utils.Singleton.wantToDisplay4G
import fr.william.antennesmobile.utils.Singleton.wantToDisplay5G

class SettingsFragment : Fragment(), CompoundButton.OnCheckedChangeListener {

    private lateinit var textDistance:TextView
    private var switch2g: SwitchCompat? = null
    private var switch3g: SwitchCompat? = null
    private var switch4g: SwitchCompat? = null
    private var switch5g: SwitchCompat? = null
    private var lDistance: LinearLayout? = null
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_settings, container, false)

        sharedPreferences = (activity as Context).getSharedPreferences("MyPreferences", MODE_PRIVATE)

        lDistance = view.findViewById(R.id.linearLayout1)
        switch2g = view.findViewById(R.id.switch_2g)
        switch3g = view.findViewById(R.id.switch_3g)
        switch4g = view.findViewById(R.id.switch_4g)
        switch5g = view.findViewById(R.id.switch_5g)
        textDistance = view.findViewById(R.id.distance_key)

        switch2g?.setOnCheckedChangeListener(this)
        switch3g?.setOnCheckedChangeListener(this)
        switch4g?.setOnCheckedChangeListener(this)
        switch5g?.setOnCheckedChangeListener(this)

        switch2g?.isChecked = wantToDisplay2G
        switch3g?.isChecked = wantToDisplay3G
        switch4g?.isChecked = wantToDisplay4G
        switch5g?.isChecked = wantToDisplay5G

        textDistance.text = (sharedPreferences.getInt("distance", 15000) / 1000).toString()

        lDistance?.setOnClickListener {
            showDialog()
        }

        return view
    }

    private fun showDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity as Context, R.style.CustomAlertDialog)
        builder.setTitle("Choisir une distance en km")

        val input = EditText(activity as Context)
        input.text = SpannableStringBuilder((sharedPreferences.getInt("distance", 10)/1000).toString())
        input.inputType = InputType.TYPE_CLASS_NUMBER
        builder.setView(input)

        builder.setPositiveButton("OK") { dialog, _ ->
            val dist = input.text.toString().toInt() * 1000
            if(dist < 300000) {
                distance = dist
                textDistance.text = input.text.toString()
                sharedPreferences.edit().putInt("distance", distance).apply()
                dialog.dismiss()
            }else {
                dialog.dismiss()
                Toast.makeText(activity, "La distance est trop grande !", Toast.LENGTH_LONG).show()
            }
            Toast.makeText(activity, "Redémarrer l'application pour appliquer ce paramètre", Toast.LENGTH_SHORT).show()
        }
        builder.setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }

        builder.show()
    }

    override fun onCheckedChanged(view: CompoundButton, isChecked: Boolean) {
        when (view.id) {
            R.id.switch_2g -> wantToDisplay2G = isChecked
            R.id.switch_3g -> wantToDisplay3G = isChecked
            R.id.switch_4g -> wantToDisplay4G = isChecked
            R.id.switch_5g -> wantToDisplay5G = isChecked
        }
        sharedPreferences.edit().putBoolean(view.id.toString(), isChecked).apply()
    }
}