package fr.william.antennesmobile.fragment

import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.william.antennesmobile.R
import fr.william.antennesmobile.activity.SiteInformation
import fr.william.antennesmobile.adapter.CellClickListener
import fr.william.antennesmobile.adapter.SitesAdapter
import fr.william.antennesmobile.utils.HttpConnectServerAsyncTask
import fr.william.antennesmobile.utils.RefreshAsyncTask
import fr.william.antennesmobile.utils.Singleton
import fr.william.antennesmobile.utils.Singleton.filterList
import fr.william.antennesmobile.utils.Singleton.getUrl
import fr.william.antennesmobile.utils.Singleton.lastLatitude
import fr.william.antennesmobile.utils.Singleton.lastLongitude
import fr.william.antennesmobile.utils.Singleton.mapList
import fr.william.antennesmobile.utils.Singleton.pages
import fr.william.antennesmobile.utils.Singleton.rowSize
import fr.william.antennesmobile.utils.Singleton.sitesList
import fr.william.antennesmobile.utils.Singleton.sitesListFilter
import java.io.IOException

class SitesFragment : Fragment(), CellClickListener, RefreshAsyncTask {

    private var isLoading: Boolean = false
    private var adapter: SitesAdapter? = null
    private var alertDialog: AlertDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_sites, container, false)

        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(activity as Context)
        adapter = SitesAdapter(this)
        recyclerView.adapter = adapter

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = LinearLayoutManager::class.java.cast(recyclerView.layoutManager)
                val totalItemCount = layoutManager.itemCount
                val lastVisible = layoutManager.findLastVisibleItemPosition()

                val endHasBeenReached = lastVisible + 3 >= totalItemCount
                if (!isLoading && totalItemCount > 0 && endHasBeenReached) {
                    if (rowSize * (pages+2) < mapList.size) {
                        pages++
                        sitesList = mapList.subList(0, rowSize * (pages+1))
                        sitesListFilter = sitesList.toMutableList()
                        filterList()
                        refresh(false)
                    }
                }
            }
        })
        return view
    }

    fun changeLocation(query: String) {
        var addressList = listOf<Address>()

        if (query !== "") {
            val geocoder = Geocoder(activity)
            try {
                addressList = geocoder.getFromLocationName(query, 1)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            if (addressList.isNotEmpty()) {
                val address = addressList[0]
                val builder = AlertDialog.Builder(activity as Context)
                builder.setTitle("Changer de ville ?")
                builder.setCancelable(false)
                builder.setMessage("Choisir la ville ${address.getAddressLine(0)} ${address.adminArea} ?")
                builder.setPositiveButton("Oui") { dialog, _ ->
                    lastLatitude = address.latitude
                    lastLongitude = address.longitude
                    dialog.dismiss()
                    updateSites()
                }
                builder.setNegativeButton("Non") { dialog, _ ->
                    dialog.cancel()
                }
                builder.show()
            }
        }
    }

    private fun updateSites() {
        showAlertDialog(activity as Context)
        HttpConnectServerAsyncTask(activity).execute(
            getUrl(true).plus("&geofilter.distance=${lastLatitude}, ${lastLongitude}, ${Singleton.distance}"),
            false,
            false
        )
    }

    private fun showAlertDialog(context: Context) {
        val loading = AlertDialog.Builder(context, R.style.CustomAlertDialog)
        loading.setCancelable(false)
        val view = LayoutInflater.from(context).inflate(R.layout.custom_progress_bar, null)
        loading.setView(view)
        alertDialog = loading.create()
        alertDialog?.show()
    }

    override fun onCellClickListener(position: Int) {
        val intent = Intent(activity as Context, SiteInformation::class.java)
        intent.putExtra("site", sitesListFilter[position])
        startActivity(intent)
        activity?.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    override fun refresh(isChangingPosition: Boolean) {
        adapter?.notifyDataSetChanged()
        alertDialog?.dismiss()
        isLoading = false
    }
}