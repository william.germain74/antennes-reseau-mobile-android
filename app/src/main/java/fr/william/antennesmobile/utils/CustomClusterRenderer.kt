package fr.william.antennesmobile.utils

import android.content.Context
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer

class CustomClusterRenderer(ctx: Context, map: GoogleMap?, cManager: ClusterManager<Sites>) : DefaultClusterRenderer<Sites>(ctx, map, cManager) {

    override fun onBeforeClusterItemRendered(item: Sites, markerOptions: MarkerOptions) {
        markerOptions.icon(getColor(item.op_name))
    }

    private fun getColor(opName: String): BitmapDescriptor {
        when (opName) {
            "SFR" -> return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)
            "Orange" -> return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)
            "Free Mobile" -> return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)
            "Bouygues Telecom" -> return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)
        }
        return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)
    }
}