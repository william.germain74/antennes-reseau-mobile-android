package fr.william.antennesmobile.utils

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem
import java.io.Serializable

data class Sites(
    var com_code: String,
    var com_name: String,
    var dep_code: String,
    var dep_name: String,
    var epci_name: String,
    var location: Coordinates,
    var distance: String,
    var op_code: String,
    var op_name: String,
    var op_site_id: String,
    var reg_code: String,
    var reg_name: String,
    var release_last_quarter_4g: String,
    var technology: String
) : Serializable, ClusterItem {
    override fun getPosition(): LatLng {
        return LatLng(location.latitude,location.longitude)
    }

    override fun getTitle(): String {
        return op_name
    }

    override fun getSnippet(): String {
        return technology
    }
}

data class Coordinates(
    var latitude: Double,
    var longitude: Double
) : Serializable