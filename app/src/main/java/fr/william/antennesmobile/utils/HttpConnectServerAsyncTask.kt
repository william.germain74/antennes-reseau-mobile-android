package fr.william.antennesmobile.utils

import android.app.Activity
import android.location.Location
import android.os.AsyncTask
import fr.william.antennesmobile.activity.MainActivity
import fr.william.antennesmobile.activity.MySplashScreen
import fr.william.antennesmobile.utils.Singleton.lastLatitude
import fr.william.antennesmobile.utils.Singleton.lastLongitude
import fr.william.antennesmobile.utils.Singleton.mapList
import fr.william.antennesmobile.utils.Singleton.rowSize
import fr.william.antennesmobile.utils.Singleton.sitesList
import fr.william.antennesmobile.utils.Singleton.sitesListFilter
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.URL
import java.text.DecimalFormat

class HttpConnectServerAsyncTask(mActivity: Activity?) : AsyncTask<Any, Int, String>() {

    private var url: String = ""
    private var isAdding: Boolean = false
    private var isSplashScreen: Boolean = false
    private var mActivity = WeakReference<Activity>(mActivity)

    override fun doInBackground(vararg param: Any?): String {
        url = param[0] as String
        isSplashScreen = param[1] as Boolean
        isAdding = param[2] as Boolean

        var jsonStr = ""
        val url = URL(url)
        val urlConnection = url.openConnection() as HttpURLConnection
        if (urlConnection.responseCode == HttpURLConnection.HTTP_OK) {
            val input = BufferedReader(InputStreamReader(urlConnection.inputStream))
            val stringBuilder = StringBuilder()
            var line: String?
            while (run {
                    line = input.readLine()
                    line
                } != null) {
                stringBuilder.append(line)
            }
            jsonStr = stringBuilder.toString()
            input.close()
        }
        urlConnection.disconnect()

        val jsonRecords = JSONObject(jsonStr).getJSONArray("records")

        var event: JSONObject
        var distance: String

        if (!isAdding)
            mapList.clear()

        for (i in 0 until jsonRecords.length()) {
            event = JSONObject(jsonRecords.getJSONObject(i).getJSONObject("fields").toString())

            val latitude = event.getJSONArray("geo_point_2d").getDouble(0)
            val longitude = event.getJSONArray("geo_point_2d").getDouble(1)

            val results = FloatArray(1)
            Location.distanceBetween(latitude, longitude, lastLatitude, lastLongitude, results)
            distance = if (results[0] >= 1000)
                DecimalFormat("#0.00").format(results[0] / 1000.0).plus(" km")
            else
                DecimalFormat("0").format(results[0]).plus(" m")

            mapList.add(
                Sites(
                    event.optString("com_code"),
                    event.optString("com_name"),
                    event.optString("dep_code"),
                    event.optString("dep_name"),
                    event.optString("epci_name"),
                    Coordinates(latitude, longitude),
                    distance,
                    event.optString("op_code"),
                    event.optString("op_name"),
                    event.optString("op_site_id"),
                    event.optString("reg_code"),
                    event.optString("reg_name"),
                    event.optString("release_last_quarter_4g"),
                    event.optString("technology")
                )
            )
        }
        return jsonStr
    }

    override fun onPostExecute(result: String) {
        if (isSplashScreen)
            (mActivity.get() as MySplashScreen).refresh(false)
        else
            (mActivity.get() as MainActivity).refresh(!isAdding && !isSplashScreen)
    }
}