package fr.william.antennesmobile.utils

interface RefreshAsyncTask {
    fun refresh(isChangingPosition :Boolean)
}