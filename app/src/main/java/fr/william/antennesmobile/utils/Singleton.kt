package fr.william.antennesmobile.utils

import android.os.Build
import java.util.function.Predicate

object Singleton {

    var pages = 0
    var rowSize = 10
    var distance = 15000
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    var wantToDisplay2G = true
    var wantToDisplay3G = true
    var wantToDisplay4G = true
    var wantToDisplay5G = true
    var lastLatitude: Double = 0.0
    var lastLongitude: Double = 0.0
    var isConnectedToInternet = true
    var mapList: MutableList<Sites> = ArrayList()
    var sitesList: MutableList<Sites> = ArrayList()
    var mapListFilter: MutableList<Sites> = ArrayList()
    var sitesListFilter: MutableList<Sites> = ArrayList()
    var opIsVisibleList = booleanArrayOf(true, true, true, true)
    var opIsVisibleMap = booleanArrayOf(true, true, true, true)
    val operators = arrayOf("SFR", "Free Mobile", "Orange", "Bouygues Telecom")

    fun setCoordinates(lat: Double, long: Double) {
        lastLatitude = lat
        lastLongitude = long
    }

    fun setCurrentCoordinates(lat: Double, long: Double) {
        currentLatitude = lat
        currentLongitude = long
    }

    fun getUrl(isMapFragment: Boolean): String {
        return if (isMapFragment)
            "https://public.opendatasoft.com/api/records/1.0/search/?dataset=buildingref-france-arcep-mobile-site-2g3g4g&q=&rows=5000&facet=op_name&facet=technology&facet=com_code&facet=com_name&facet=epci_name&facet=epci_code&facet=dep_name&facet=dep_code&facet=reg_name&facet=reg_code"
        else
            "https://public.opendatasoft.com/api/records/1.0/search/?dataset=buildingref-france-arcep-mobile-site-2g3g4g&q=&rows=$rowSize&start=${pages * rowSize}&facet=op_name&facet=technology&facet=com_code&facet=com_name&facet=epci_name&facet=epci_code&facet=dep_name&facet=dep_code&facet=reg_name&facet=reg_code"
    }

    private fun <Sites> remove(list: MutableList<Sites>, predicate: Predicate<Sites>) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val newList: MutableList<Sites> = ArrayList()
            list.filter { predicate.test(it) }.forEach { newList.add(it) }
            list.removeAll(newList)
        }
    }

    fun filterList() {
        val removeOperator = Predicate<Sites> { sites ->
            sites.op_name == operators[0] && !opIsVisibleList[0] || sites.op_name == operators[1] && !opIsVisibleList[1]
                    || sites.op_name == operators[2] && !opIsVisibleList[2] || sites.op_name == operators[3] && !opIsVisibleList[3]
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            remove(sitesListFilter, removeOperator)
        }
    }

    fun filterMap() {
        val removeOperator = Predicate<Sites> { sites ->
            sites.op_name == operators[0] && !opIsVisibleMap[0] || sites.op_name == operators[1] && !opIsVisibleMap[1]
                    || sites.op_name == operators[2] && !opIsVisibleMap[2] || sites.op_name == operators[3] && !opIsVisibleMap[3]
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            remove(mapListFilter, removeOperator)
        }
    }
}